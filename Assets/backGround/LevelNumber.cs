﻿using UnityEngine;
using System.Collections;

public class LevelNumber : MonoBehaviour {

    private static int levelNumber = 1;

	void Start () {
	
	}
	
	void Update () {
	
	}

    public int getLevelNumber() {
        return levelNumber;
    }

    public void incLevelNumber() {
        levelNumber++;
    }
}
