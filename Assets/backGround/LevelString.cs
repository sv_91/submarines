﻿using UnityEngine;
using System.Collections;

public class LevelString : MonoBehaviour {

    private int levelNumber;

	void Start () {
        levelNumber = GameObject.Find("LevelNumber").GetComponent<LevelNumber>().getLevelNumber();
	}
	
	void Update () {
        GetComponent<GUIText>().text = "Level: " + levelNumber;
	}
}
