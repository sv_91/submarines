﻿using UnityEngine;
using System.Collections;

public class Lives : MonoBehaviour {

    public int maxCountLives;
	public static int lives = 3;
	public GameObject liveTemplate;
	private GameObject[] realLives;

	void Start () {
        realLives = new GameObject[maxCountLives];
		for (int i = 0; i < maxCountLives; i++) {
			realLives[i] = (GameObject)Instantiate(
				liveTemplate,
				this.transform.position + new Vector3((maxCountLives - i - 1) * liveTemplate.GetComponent<SpriteRenderer>().bounds.size.x, 0, 0),
				this.transform.rotation
			);
		}
		for (int i = lives; i < maxCountLives; i++) {
			realLives[i].SetActive(false);
		}
	}

	void Update () {
	
	}

    public int getCountLives() {
        return lives;
    }

	public int getMaxCountLives() {
		return maxCountLives;
	}

    public void plusLive() {
        if (lives < maxCountLives) {
            realLives[lives].SetActive(true);
            lives++;
        }
    }

    public bool minusLive() {
        if (lives == 0) {
            return false;
        } else {
            lives--;
            realLives[lives].SetActive(false);
            return true;
        }
    }
}
