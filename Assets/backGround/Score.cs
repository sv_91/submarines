﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public static int score = 100;

	void Update() {
		GetComponent<GUIText>().text = "Score: " + score;
	}

    public void plusScore(int value) {
        score += value;
    }

    public int getScore() {
        return score;
    }
}
