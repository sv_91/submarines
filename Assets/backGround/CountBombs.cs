﻿using UnityEngine;
using System.Collections;

public class CountBombs : MonoBehaviour {

	public int maxBombCount;
	public int costBomb;

	public GameObject bombTemplate;

	private int currentBombCount;
	private GameObject[] realBombs;
	private Score score;

	void Start () {
		score = GameObject.Find("Score").GetComponent<Score>();

		realBombs = new GameObject[maxBombCount];

		currentBombCount = 0;
		for (int i = 0; i < maxBombCount; i++) {
			realBombs[i] = (GameObject)Instantiate(
				bombTemplate, 
				this.transform.position + new Vector3(currentBombCount * bombTemplate.GetComponent<SpriteRenderer>().bounds.size.x, 0, 0), 
				this.transform.rotation
			);
			currentBombCount++;
		}
	}
	
	public void restoreBomb() {
		if (currentBombCount < maxBombCount) {
			if (realBombs[currentBombCount] != null) { // Защита от обращения после destroy-я
				realBombs[currentBombCount].SetActive(true);
			}
            currentBombCount++;
		}
	}

	public bool destroyBomb() {
		if (score.getScore() < costBomb) {
			return false;
		}
		if (currentBombCount == 0) {
			return false;
		} else {
			score.plusScore(-costBomb);
			currentBombCount--;
			realBombs[currentBombCount].SetActive(false);
			return true;
		}
	}

	void Update () {
	}
}
