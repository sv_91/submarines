﻿using UnityEngine;
using System.Collections;

public class Buttons : MonoBehaviour {

	public int widthBox;

	public int widthButton;
	public int heightButton;
	public int plusHeightButton;

	public int fontSize;

	private int countButtons = 1;
	private int heightBox;
	
	void Start () {
		heightBox = (fontSize + plusHeightButton) + (heightButton + plusHeightButton) * countButtons + plusHeightButton / 2;
	}
	
	void Update () {
	
	}

	private Rect getButtonRect(int numberButton) {
		int beginBoxY = (Screen.height - heightBox) / 2;
		return new Rect(
			(Screen.width - widthButton) / 2,
			beginBoxY + (fontSize + plusHeightButton) + (heightButton + plusHeightButton) * numberButton, 
			widthButton, 
			heightButton
		);
	}

    void OnGUI() {
		GUIStyle guiStyleBox = new GUIStyle(GUI.skin.box);
		guiStyleBox.fontSize = fontSize;

		GUIStyle guiStyleButton = new GUIStyle(GUI.skin.button);
		guiStyleButton.fontSize = fontSize;

        // Make a background box
		GUI.Box(
			new Rect((Screen.width - widthBox) / 2, (Screen.height - heightBox) / 2, widthBox, heightBox),
			"Loader Menu",
			guiStyleBox
		);

        // Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if (GUI.Button(getButtonRect(0), "New Game", guiStyleButton)) {
            Application.LoadLevel("scene_pause");
        }
    }

}
