﻿using UnityEngine;
using System.Collections;

public class CountEnemy : MonoBehaviour {

    private LevelNumber levelNumber;

	void Start () {
        levelNumber = GameObject.Find("LevelNumber").GetComponent<LevelNumber>();
	}
	
	void Update () {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0 && GameObject.FindGameObjectsWithTag("SubBomb").Length == 0) {
			Debug.Log("Next level");
            levelNumber.incLevelNumber();
            Application.LoadLevel("scene_pause");
        }
	}
}
