﻿using UnityEngine;
using System.Collections;

public class ScenePause : MonoBehaviour {

    private int levelNumber;

	void Start () {
        levelNumber = GameObject.Find("LevelNumber").GetComponent<LevelNumber>().getLevelNumber();
	}
	
	void Update () {
		if (Input.anyKeyDown) {
			Application.LoadLevel("scene_" + levelNumber);
		}
	}
}
