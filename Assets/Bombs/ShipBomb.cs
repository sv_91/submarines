﻿using UnityEngine;
using System.Collections;

public class ShipBomb : MonoBehaviour {

	private CountBombs countBombs;

	private float forceX = 0;

	void Start () {
		countBombs = GameObject.Find("CountBombs").GetComponent<CountBombs>();
		GetComponent<Rigidbody2D>().AddForce(new Vector2(forceX, 0));
	}

	public void setForce(float force) {
		this.forceX = force * 5;
	}

	void Update () {
	
	}

	void OnDestroy() {
		countBombs.restoreBomb();
	}

	void OnTriggerEnter2D (Collider2D other)	{
		if (other.name == "Bottom") {
			Destroy(this.gameObject);
		} else if (other.tag == "Enemy") {
			Destroy(this.gameObject);
		} else if (other.tag == "SubBomb") {
			Destroy(this.gameObject);
		}
	}
}
