﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

namespace Inner { 
public class Random {

	private System.Random random;
	private static int seed = Environment.TickCount;

	public Random() {
		random = new System.Random((int)(System.DateTime.Today.Ticks % 65000) * Interlocked.Increment(ref seed));
	}

	public float nextRnd(float first, float last) {
		if (last < first) {
			float buf = last;
			last = first;
			first = buf;
		}
		return (float)(random.NextDouble() * (last - first) + first);
	}

	public int nextRnd(int first, int last) {
		if (last < first) {
			int buf = last;
			last = first;
			first = buf;
		}
		return (int)(random.NextDouble() * (last - first) + first);
	}

	public int nextSign() {
		int res = nextRnd(0, 2);
		if (res == 0) {
			res = -1;
		}
		return res;
	}

}
}
