﻿using UnityEngine;
using System.Collections;
using System;

public class RandomMoving : MonoBehaviour {
	public float minTimeBetweenSpawns; // Через какое время рыба примет решение сменить скорость
	public float maxTimeBetweenSpawns; // Через какое время рыба примет решение сменить скорость
	public float minVelocityX; // минимальная скорость по координате x
	public float maxVelocityX; // максимальная скорость по координате x
	public float minVelocityY; // минимальная скорость по координате y
	public float maxVelocityY; // максимальная скорость по координате y

	private Inner.Random random;

	private float minBorderX; // Левая граница, левее которой не может опускаться рыба
	private float maxBorderX; // Правая граница, правее которой не может опускаться рыба
	private float minBorderY; // Нижняя граница, ниже которой не может опускаться рыба
	private float maxBorderY; // Верхняя граница, выше которой не может опускаться рыба

	private bool isRight_ = true;

	void setBorder(ref float minBorder, ref float maxBorder, float minSide, float maxSide, float margin) {
		maxBorder = maxSide;
		minBorder = minSide;
		float raznBorder = maxSide - minSide;
		maxBorder -= raznBorder / margin;
		minBorder += raznBorder / margin;
	}

	protected bool isRight() {
		return isRight_;
	}

	void flip() {
		if (GetComponent<Rigidbody2D>().velocity.x > 0 && !isRight_ || GetComponent<Rigidbody2D>().velocity.x < 0 && isRight_) {
			isRight_ = !isRight_;

			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}
	
	protected void Start() {
		random = new Inner.Random();

		setBorder(
			ref minBorderX, ref maxBorderX,
			GameObject.Find("LeftSide").GetComponent<BoxCollider2D>().offset.x, GameObject.Find("RightSide").GetComponent<BoxCollider2D>().offset.x,
			20
		);

		setBorder(
			ref minBorderY, ref maxBorderY,
			GameObject.Find("Bottom").GetComponent<BoxCollider2D>().offset.y, GameObject.Find("Floor").GetComponent<BoxCollider2D>().offset.y,
			20
		);

		StartCoroutine("Spawn");
	}

	private void refreshVelocity(float newVelocityX, float newVelocityY) {
		GetComponent<Rigidbody2D>().velocity = new Vector2(newVelocityX, newVelocityY);
		flip();
	}

	IEnumerator Spawn() {
		while (true) {
			float velocityX = random.nextRnd(minVelocityX, maxVelocityX) * random.nextSign();
			float velocityY = random.nextRnd(minVelocityY, maxVelocityY) * random.nextSign();

			refreshVelocity(velocityX, velocityY);

			float waitTime = random.nextRnd(minTimeBetweenSpawns, maxTimeBetweenSpawns);
			yield return new WaitForSeconds(waitTime);
		}
	}

	void FixedUpdate() {
		float currPosY = this.GetComponent<Transform>().position.y;
		if (currPosY <= minBorderY || currPosY >= maxBorderY) {
			if (currPosY <= minBorderY) {
				refreshVelocity(GetComponent<Rigidbody2D>().velocity.x, Math.Abs(GetComponent<Rigidbody2D>().velocity.y));
			} else {
				refreshVelocity(GetComponent<Rigidbody2D>().velocity.x, -Math.Abs(GetComponent<Rigidbody2D>().velocity.y));
			}
		}
		float currPosX = this.GetComponent<Transform>().position.x;
		if (currPosX <= minBorderX || currPosX >= maxBorderX) {
			if (currPosX <= minBorderX) {
				refreshVelocity(Math.Abs(GetComponent<Rigidbody2D>().velocity.x), GetComponent<Rigidbody2D>().velocity.y);
			} else {
				refreshVelocity(-Math.Abs(GetComponent<Rigidbody2D>().velocity.x), GetComponent<Rigidbody2D>().velocity.y);
			}
		}
	}

}
