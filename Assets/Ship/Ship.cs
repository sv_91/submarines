﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	public float maxSpeed; // Максимальная скорость.
    public float speedTouch; // Множитель для нажатия с тачскрина
    public float speedKey; // Множитель для нажатия с клавиатуры
    public float propScreen;

	public GameObject bombTemplate;

	private CountBombs countBombs;
    private Lives lives;

	private float currSpeed;

	private bool isRight = true;

	void Start () {
		currSpeed = 0.0f;
		countBombs = GameObject.Find("CountBombs").GetComponent<CountBombs>();
        lives = GameObject.Find("Lives").GetComponent<Lives>();
	}

    void throwBomb() {
        if (countBombs.destroyBomb()) {
            Transform shipBottom = GameObject.Find("ShipBottom").GetComponent<Transform>();
			GameObject bomb = (GameObject)Instantiate(bombTemplate, shipBottom.position, this.transform.rotation);
			bomb.GetComponent<ShipBomb>().setForce(GetComponent<Rigidbody2D>().velocity.x);
        }
    }

    void move(float score) {
        float moveForce = 365f;

        this.GetComponent<Rigidbody2D>().AddForce(Vector2.left * score * moveForce);
		if (this.GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed) {
			this.GetComponent<Rigidbody2D>().velocity = this.GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}
    }

	void FixedUpdate() {
		if (currSpeed != 0.0f) {
			move(currSpeed);
			currSpeed = 0.0f;
		}
	}

	void Update() {
        // Мобильный ввод
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
                if (touch.position.x <= Screen.width / propScreen) {
                    currSpeed = speedTouch;
					if (!isRight) {
						flip();
					}
                } else if (touch.position.x >= Screen.width - Screen.width / propScreen) {
                    currSpeed = -speedTouch;
					if (isRight) {
						flip();
					}
				} 
            } else if (touch.phase == TouchPhase.Began) {
                if (touch.position.x > Screen.width / propScreen && touch.position.x < Screen.width - Screen.width / propScreen) {
                    throwBomb();
                }
            }
		}

		// Клавиатурный ввод
		if (Input.GetAxis("Horizontal") != 0) {
			float h = Input.GetAxis("Horizontal");
			if (h > 0 && isRight) {
				flip();
			} else if (h < 0 && !isRight) {
				flip();
			}
            currSpeed = -h * speedKey;
		}

		// Сброс бомбы
		if (Input.GetButtonDown("Vertical")) {
            throwBomb();
		}
	}

	void flip() {
		isRight = !isRight;

		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    void destroy() {
        Destroy(this.gameObject);
        if (lives.minusLive()) {
            Application.LoadLevel("scene_pause");
        } else {
            Application.LoadLevel("scene_game_over");
        }
    }

	void OnTriggerEnter2D(Collider2D other)	{
		if (other.tag == "SubBomb") {
            destroy();
		}
	}
}
