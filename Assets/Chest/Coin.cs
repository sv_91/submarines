﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	public int countScore;

	private Score score;

	void Start () {
		score = GameObject.Find("Score").GetComponent<Score>();
	}
	
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			score.plusScore(countScore);
			Destroy(this.gameObject);
		} else if (other.name == "Floor2") {
			Destroy(this.gameObject);
		}
	}
}
