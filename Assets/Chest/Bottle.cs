﻿using UnityEngine;
using System.Collections;

public class Bottle : MonoBehaviour {

	private Lives lives;

	void Start () {
		lives = GameObject.Find("Lives").GetComponent<Lives>();
	}
	
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Player") {
			lives.plusLive();
			Destroy(this.gameObject);
		} else if (other.name == "Floor2") {
			Destroy(this.gameObject);
		}
	}
}
