﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour {

	public int minTimeBetweenPrize;
	public int maxTimeBetweenPrize;

	public GameObject chestOpen;
	public GameObject coin;
	public GameObject bottle;

	static private int prevLevel = 0;

	private Lives lives;

	private Inner.Random random;


	void Start () {
		lives = GameObject.Find("Lives").GetComponent<Lives>();

		random = new Inner.Random();

		LevelNumber ln = GameObject.Find("LevelNumber").GetComponent<LevelNumber>();
		if (prevLevel < ln.getLevelNumber()) {
			StartCoroutine("ThrowPrize");
		} else {
			createOpen();
		}
	}

	private void createOpen() {
		Instantiate(chestOpen, this.transform.position, this.transform.rotation);
		LevelNumber ln = GameObject.Find("LevelNumber").GetComponent<LevelNumber>();
		prevLevel = ln.getLevelNumber();
		Destroy(this.gameObject);
	}

	IEnumerator ThrowPrize() {
		float waitTime = random.nextRnd(minTimeBetweenPrize, maxTimeBetweenPrize);
		yield return new WaitForSeconds(waitTime);

		if (lives.getCountLives() < lives.getMaxCountLives()) {
			Instantiate(
				bottle,
				this.transform.position + new Vector3(0, this.GetComponent<SpriteRenderer>().bounds.size.y / 2),
				this.transform.rotation
			);
		} else { 
			Instantiate(
				coin,
				this.transform.position + new Vector3(0, this.GetComponent<SpriteRenderer>().bounds.size.y / 2),
				this.transform.rotation
			);
		}

		createOpen();
	}

	void Update () {
	
	}
}
