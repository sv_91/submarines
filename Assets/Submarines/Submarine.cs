﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class Submarine : MonoBehaviour {

	public float minTimeBetweenSpawns; // Через какое время субмарина примет решение сменить скорость
	public float maxTimeBetweenSpawns; // Через какое время субмарина примет решение сменить скорость
	public float minTimeBetweenBomb;
	public float maxTimeBetweenBomb;
	public float minRangeVelocity;
	public float maxRangeVelocity;

	public GameObject bombTemplate;

	public int countScore;

	private float velocity = 0;

	private Score score;

	private Inner.Random random;

	private bool isRight = true;

	void Start () {
		isRight = true;
		score = GameObject.Find("Score").GetComponent<Score>();

		random = new Inner.Random();

		// Start the Spawn coroutine.
		StartCoroutine("Spawn");
		StartCoroutine("Bomb");
	}

	private void refreshVelocity(float newVelocityX) {
		GetComponent<Rigidbody2D>().velocity = new Vector2(newVelocityX, 0);
		flip();
	}

	void flip() {
		if (GetComponent<Rigidbody2D>().velocity.x > 0 && !isRight || GetComponent<Rigidbody2D>().velocity.x < 0 && isRight) { 
			isRight = !isRight;

			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}

	IEnumerator Spawn() {
		while (true) {
			velocity = random.nextRnd(minRangeVelocity, maxRangeVelocity);
			if (random.nextRnd(0f, 1f) > 0.5f) {
				velocity *= -1;
			}

			refreshVelocity(velocity);

			float waitTime = random.nextRnd(minTimeBetweenSpawns, maxTimeBetweenSpawns);
			yield return new WaitForSeconds(waitTime);
		}
	}

	IEnumerator Bomb() {
		while (true) {
			float waitTime = random.nextRnd(minTimeBetweenBomb, maxTimeBetweenBomb);
			yield return new WaitForSeconds(waitTime);

			Instantiate(bombTemplate, this.transform.position, this.transform.rotation);
		}
	}

	void Update () {
	}

	private void destroy() {
		score.plusScore(countScore);
		Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other)	{
		if (other.name == "LeftSide2" || other.name == "RightSide2")	{
			velocity *= -1;

			refreshVelocity(velocity);
		} else if (other.tag == "ShipBomb") {
			destroy();
		}
	}
}
