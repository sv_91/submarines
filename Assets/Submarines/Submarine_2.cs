﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class Submarine_2 : MonoBehaviour {

	public float minTimeBetweenSpawns; // Через какое время субмарина примет решение сменить скорость
	public float maxTimeBetweenSpawns; // Через какое время субмарина примет решение сменить скорость
	public float minTimeBetweenBomb;
	public float maxTimeBetweenBomb;
	public float minVelocityX;
	public float maxVelocityX;
	public float minVelocityY;
	public float maxVelocityY;

	public GameObject bombTemplate;

	public int countScore;

	private float velocityX = 0;

	private Score score;

	private Inner.Random random;

	private float minBorder; // Нижняя граница, ниже которой не может опускаться субмарина
	private float maxBorder; // Нижняя граница, ниже которой не может опускаться субмарина
	private float currBorder;
	private float beginOffset;

	private bool isRight = true;


	void Start() {
		isRight = true;
		score = GameObject.Find("Score").GetComponent<Score>();

		random = new Inner.Random();

		maxBorder = GameObject.Find("Floor").GetComponent<BoxCollider2D>().offset.y;
		minBorder = GameObject.Find("Bottom").GetComponent<BoxCollider2D>().offset.y;
		float raznBorder = maxBorder - minBorder;
		maxBorder -= raznBorder / 10;
		minBorder += raznBorder / 10;

		currBorder = this.GetComponent<Transform>().position.y;

		// Start the Spawn coroutine.
		StartCoroutine("Spawn");
		StartCoroutine("Bomb");
	}

	void flip() {
		if (GetComponent<Rigidbody2D>().velocity.x > 0 && !isRight || GetComponent<Rigidbody2D>().velocity.x < 0 && isRight) {
			isRight = !isRight;

			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}
	
	private void refreshVelocity(float newVelocityX, float newVelocityY) {
		GetComponent<Rigidbody2D>().velocity = new Vector2(newVelocityX, newVelocityY);
		flip();
	}

	IEnumerator Spawn() {
		while (true) {
			velocityX = random.nextRnd(minVelocityX, maxVelocityX);
			if (random.nextRnd(0f, 1f) > 0.5f) {
				velocityX *= -1;
			}

			if (random.nextRnd(0f, 1f) > 0.5f) {
				currBorder = random.nextRnd(minBorder, maxBorder);
			}

			beginOffset = this.GetComponent<Transform>().position.y - currBorder;
			float velocityY = random.nextRnd(minVelocityY, maxVelocityY) * Math.Sign(-beginOffset);

			refreshVelocity(velocityX, velocityY);

			float waitTime = random.nextRnd(minTimeBetweenSpawns, maxTimeBetweenSpawns);
			yield return new WaitForSeconds(waitTime);
		}
	}

	IEnumerator Bomb() {
		while (true) {
			float waitTime = random.nextRnd(minTimeBetweenBomb, maxTimeBetweenBomb);
			yield return new WaitForSeconds(waitTime);

			Instantiate(bombTemplate, this.transform.position, this.transform.rotation);
		}
	}

	void FixedUpdate() {
		float currOffset = this.GetComponent<Transform>().position.y - currBorder;
		if (currOffset * beginOffset <= 0) {
			if (GetComponent<Rigidbody2D>().velocity.y != 0) { 
				refreshVelocity(GetComponent<Rigidbody2D>().velocity.x, 0);
			}
		}
	}

	private void destroy() {
		score.plusScore(countScore);
		Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.name == "LeftSide2" || other.name == "RightSide2") {
			velocityX *= -1;

			refreshVelocity(velocityX, 0);
		} else if (other.tag == "ShipBomb") {
			destroy();
		}
	}
}
