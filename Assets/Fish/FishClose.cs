﻿using UnityEngine;
using System.Collections;
using System;

public class FishClose : RandomMoving {
	public GameObject fishOpen;

	void Start() {
		base.Start();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "ShipBomb" || other.tag == "SubBomb" || other.tag == "Prize") {
			other.GetComponent<Rigidbody2D>().Sleep();
			GameObject fishOpenG = (GameObject)Instantiate(fishOpen, this.transform.position, this.transform.rotation);
			fishOpenG.GetComponent<FishOpen>().setObj(other);
			fishOpenG.GetComponent<FishOpen>().setRight(base.isRight());
			Destroy(this.gameObject);
		}
	}
}
