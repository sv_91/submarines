﻿using UnityEngine;
using System.Collections;

public class FishOpen : MonoBehaviour {

	public float timeClose;
	public GameObject fishClose;

	private volatile Collider2D obj = null;

	private bool isRight = true;

	void Start () {
		StartCoroutine("Close");
		flip();
	}

	void flip() {
		if (!isRight) {
			Vector3 theScale = transform.localScale;
			theScale.x = -1 * Mathf.Abs(theScale.x);
			transform.localScale = theScale;
		}
	}

	IEnumerator Close() {
		yield return new WaitForSeconds(timeClose);
		Instantiate(fishClose, this.transform.position, this.transform.rotation);
		if (obj != null) {
			Destroy(obj.gameObject);
			obj = null;
		}
		Destroy(this.gameObject);
	}

	void Update () {
	}

	public Collider2D getObj() {
		return this.obj;
	}

	public void setObj(Collider2D obj) {
		this.obj = obj;
	}

	public void setRight(bool isRight) {
		this.isRight = isRight;
		flip();
	}

	void OnDestroy() {
		if (obj != null) { 
			obj.GetComponent<Rigidbody2D>().WakeUp();
		}
	}

}
