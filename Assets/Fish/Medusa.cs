﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;

public class Medusa : RandomMoving {

	void Start() {
		base.Start();
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Fish") {
			Destroy(other.gameObject);
		}
	}
}
